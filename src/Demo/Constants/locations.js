export default [
  {
    timezone: "America/Los_Angeles",
    title: "California"
  },
  {
    timezone: "Europe/London",
    title: "London"
  },
  {
    timezone: "Asia/Bangkok",
    title: "Thailand"
  }
]
