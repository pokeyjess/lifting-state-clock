import React from 'react';
import Clock from './Clock'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (<div>
      {
      this.props.locations.map((val) => {
        return (<Clock 
                  key={val.location}
                  location={val.location}
                  timezone={val.timezone}
                  />)
          }
        )
      }
      </div>
    )
  }
}

export default Dashboard