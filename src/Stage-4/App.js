import React from 'react';
import Dashboard from './Components/'
import './App.css';

import locations from './locations'

function App() {
  return (
    <div className="App">
      <Dashboard locations={locations}/>
    </div>
  );
}

export default App;

